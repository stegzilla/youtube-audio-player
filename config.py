import json
import time
import threading
import logging as log
import os

log.basicConfig(level=log.INFO,
                    format='%(asctime)s:\n'
                    '%(levelname)s: '    
                    '%(module)s.'
                    '%(funcName)s-'
                    '%(lineno)d: '
                    '%(message)s')

path = None
conf = None

def config_path():
	return str(os.path.join(path, 'config.json'))

def queue_path():
	return os.path.join(path, 'queue.json')

defaults = {
	"web":{
		"port":8888,
		"restrictSkip":False,
		"ipNames":[{"ip":"127.0.0.1","name":"Local"}],
		"whitelist":[],
		"searchDefault":"ytsc",
		"debug":False,
		"requireLogin":False,
		"ldap":{
			"server":"",
			"domain":"",
			"scope":""
		},
		"title": "Music"
	},
	"gmusic":{
		"enable":False,
		"user":"",
		"password":"",
		"download":False,
		"device_id": "mac"
	},
	"misc":{
		"maxLength":600,
		"soundDevice":"PCM"
	}
}

def load():
	global conf
	try:
		log.info("loading config")
		f = open(config_path(), "r")
		data = json.load(f)
		f.close()

		for key in defaults:
			if key not in data:
				data[key] = defaults[key]

			for subkey in defaults[key]:
				if subkey not in data[key]:
					data[key][subkey] = defaults[key][subkey]

		if not conf == data:
			f = open(config_path(), "w")
			json.dump(data, f, indent = 4)
			f.close()

		conf = data

		log.info("done")
	except IOError:
		log.info("couldn't read config file, creating...")
		conf = defaults
		f = open(config_path(), "w")
		json.dump(defaults, f, indent = 4)
		f.close()

		log.info("done")

	#thread = threading.Thread(target = refresh)
	#thread.start()

def refresh():
	time.sleep(30)
	load()