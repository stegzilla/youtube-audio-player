from datetime import datetime

import db


def get_subclasses(m, bc):
    r = []
    bcn = [c.__name__ for c in bc]
    cs = [c for c in dir(m) if c not in bcn]  # Get list of classes except base_classes

    for cn in cs:
        c = getattr(m, cn)
        try:
            for b in bc:
                if issubclass(c, b):
                    r.append(c)
                    break
        except TypeError:  # If 'obj' is not a class
            pass
    return r


models = get_subclasses(db, [db.BaseModel, db.TrackModel])
db.db.create_tables(models)
