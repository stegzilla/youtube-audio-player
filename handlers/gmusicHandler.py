from gmusicapi import Mobileclient
from gmusicapi import CallFailure
import logging as log
import time
import order
import threading

log.basicConfig(level=log.INFO,
                format='%(asctime)s:\n'
                       '%(levelname)s: '
                       '%(module)s.'
                       '%(funcName)s-'
                       '%(lineno)d: '
                       '%(message)s')

clients = {}


class GMusic:
    def __init__(self, account):
        self.library = []
        self.playlists = []
        self.lastLoad = None
        self.pLastLoad = None
        self.api = Mobileclient(debug_logging=False)
        device_id = account.extras.get("device_id", None)
        if not device_id or device_id == "mac":
            device_id = Mobileclient.FROM_MAC_ADDRESS
        self.api.login(account.username, account.password, device_id)

        if not self.api.is_authenticated():
            self.api = None
            log.error("failed to login gmusic")
            return

    def loadLibrary(self):
        if not self.library or self.lastLoad < int(time.time()):
            self.library = self.api.get_all_songs()
            self.lastLoad = int(time.time()) + 60

    def loadPlaylists(self):
        if not self.playlists or self.pLastLoad < int(time.time()):
            self.playlists = self.api.get_all_user_playlist_contents()
            self.pLastLoad = int(time.time()) + 60

    def search(self, query):
        qresults = self.api.search(query, max_results=50)
        if not qresults["song_hits"]:
            return []
        results = []
        for result in qresults["song_hits"]:
            results.append({
                "id": result["track"]["nid"],
                "title": result["track"]["title"],
                "artist": result["track"]["artist"],
                "album": result["track"]["album"],
                "albumId": result["track"]["albumId"],
                "duration": int(result["track"]["durationMillis"]),
                "explicit": result["track"]["explicitType"]
            })
        return results

    def searchLibrary(self, query):
        self.loadLibrary()

        query = [query]
        track_results = None
        results = []

        for value in query:
            q = value.strip().lower()

            def id_filter(track):
                return q in track["id"]

            def track_name_filter(track):
                return q in track["title"].lower()

            def album_filter(track):
                return q in track["album"].lower()

            def artist_filter(track):
                return q in track["artist"].lower()

            def any_filter(track):
                return any([
                    id_filter(track),
                    track_name_filter(track),
                    album_filter(track),
                    artist_filter(track)
                ])

            track_results = filter(any_filter, self.library)

        for result in track_results:
            if not "albumId" in result:
                albumId = None
            else:
                albumId = result["albumId"]
            results.append({
                "id": result["id"],
                "title": result["title"],
                "artist": result["artist"],
                "album": result["album"],
                "albumId": albumId,
                "duration": int(result["durationMillis"]),
                "explicit": "3"
            })

        return results

    def getInfo(self, trackId, again=False):
        try:
            if trackId.startswith("T"):
                track = self.api.get_track_info(trackId)
                artist = track["artist"]
                title = u"{0} - {1} ({2})".format(artist, track["title"], track["album"])
                result = {
                    "id": trackId,
                    "title": title,
                    "length": int(track["durationMillis"]) / 1000,
                    "durationMillis": track["durationMillis"],
                    "artist": track["artist"],
                    "album": track["album"],
                    "albumId": track["albumId"],
                    "name": track["title"]
                }
                return result
            else:
                self.loadLibrary()

                track = [track for track in self.library if str(track["id"]) == trackId]
                if track:
                    track = track[0]
                    artist = track["artist"]
                    if not "albumId" in track:
                        albumId = None
                    else:
                        albumId = track["albumId"]
                    title = "%s - %s (%s)" % (artist, track["title"], track["album"])
                    result = {
                        "id": trackId,
                        "title": title,
                        "length": int(track["durationMillis"]) / 1000,
                        "durationMillis": track["durationMillis"],
                        "artist": track["artist"],
                        "album": track["album"],
                        "albumId": albumId,
                        "name": track["title"]
                    }
                    return result
                raise ValueError
        except ValueError:
            if not again:
                newTrackId = "T" + trackId
                return self.getInfo(newTrackId, again=True)
            return {"id": 0}

    def getStreamUrl(self, trackId):
        t = threading.Thread(target=self.api.increment_song_playcount, args=(trackId,))
        t.start()
        return "ffmpeg://" + self.api.get_stream_url(trackId)

    def getAlbum(self, albumId, source):
        try:
            if source == "gmusic":
                album = self.api.get_album_info(albumId)

                tracks = []
                for track in album["tracks"]:
                    tracks.append({
                        "id": track["nid"],
                        "title": track["title"],
                        "artists": track["artist"],
                        "length": time.strftime('%M:%S', time.gmtime(int(track["durationMillis"]) / 1000))
                    })

                result = {
                    "id": albumId,
                    "artist": album["artist"],
                    "albumName": album["name"],
                    "total": len(album["tracks"]),
                    "albumArt": album["albumArtRef"],
                    "tracks": tracks
                }
            else:
                self.loadLibrary()

                album = None

                for track in self.library:
                    if "albumId" not in track:
                        continue
                    elif str(track["albumId"]) == albumId:
                        album = track
                        break

                if not album:
                    raise ValueError

                tracksResult = sorted([track for track in self.library if
                                       track["album"] == album["album"] and track["albumArtist"] == album[
                                           "albumArtist"]],
                                      key=lambda track: track["trackNumber"])

                tracks = []
                for track in tracksResult:
                    tracks.append({
                        "id": track["id"],
                        "title": track["title"],
                        "artists": track["artist"],
                        "length": time.strftime('%M:%S', time.gmtime(int(track["durationMillis"]) / 1000))
                    })

                result = {
                    "id": albumId,
                    "artist": tracksResult[0]["artist"],
                    "albumName": tracksResult[0]["album"],
                    "total": len(tracks),
                    "albumArt": tracksResult[0]["albumArtRef"][0]["url"],
                    "tracks": tracks
                }

            return result
        except ValueError:
            return {"id": 0}
        except CallFailure:
            return {"id": 0}

    def addAlbum(self, albumId, source, user):
        try:
            album = self.getAlbum(albumId, source)
            if album:
                tracks = []
                for track in album["tracks"]:
                    item = order.QueueItem(str(track["id"]), source)
                    if not item.error:
                        tracks.append(item)
                order.bulk_add(tracks, user)
                return True
        except Exception as e:
            log.warn("failed to add album: " + e.message)
            return False

    def getPlaylists(self):
        self.loadPlaylists()

        return [{"name": playlist["name"], "id": playlist["id"]} for playlist in self.playlists]

    def getPlaylist(self, pId, source):
        self.loadPlaylists()

        pl = None

        for playlist in self.playlists:
            if playlist["id"] == pId:
                pl = playlist

        tracks = []
        for track in pl["tracks"]:
            trackResult = self.getInfo(str(track["trackId"]))

            tracks.append({
                "id": trackResult["id"],
                "title": trackResult["name"],
                "artist": trackResult["artist"],
                "length": time.strftime('%M:%S', time.gmtime(int(trackResult["durationMillis"]) / 1000)),
                "album": trackResult["album"],
                "albumId": trackResult["albumId"]
            })

        result = {
            "id": pl["id"],
            "name": pl["name"],
            "tracks": tracks,
            "total": len(tracks)
        }

        return result

    def getPlaylistLite(self, pId, source):
        self.loadPlaylists()

        pl = None

        for playlist in self.playlists:
            if playlist["id"] == pId:
                pl = playlist

        result = {
            "id": pl["id"],
            "name": pl["name"],
            "tracks": [track["trackId"] for track in pl["tracks"]]
        }

        return result

    def addPlaylist(self, pId, source, user):
        try:
            playlist = self.getPlaylistLite(pId, source)
            if playlist:
                tracks = []
                for track in playlist["tracks"]:
                    item = order.QueueItem(str(track), source)
                    if not item.error:
                        tracks.append(item)
                order.bulk_add(tracks, user)
                return True
        except Exception as e:
            log.warn("failed to add playlist: " + e.message)
            return False

    def getSituations(self, chunk=False):
        data = self.api.get_listen_now_situations()
        result = []
        for sit in data:
            try:
                result.append({
                    "id": sit["id"],
                    "img": sit["imageUrl"],
                    "desc": sit["description"],
                    "title": sit["title"],
                    "stations": sit["stations"]
                })
            except KeyError:
                if u'situations' in sit:
                    for sit2 in sit["situations"]:
                        try:
                            result.append({
                                "id": sit2["id"],
                                "img": sit2["imageUrl"],
                                "desc": sit2["description"],
                                "title": sit2["title"],
                                "stations": sit2["stations"]
                            })
                        except KeyError:
                            continue
        stations = self.getStations()
        for sit in result:
            for station in sit["stations"]:
                name = station["name"]
                check = [s["id"] for s in stations if s["name"] == name]
                if not check:
                    log.debug("creating " + name)
                    csId = self.api.create_station(name=name, curated_station_id=station["seed"]["curatedStationId"])
                else:
                    csId = check[0]
                station["id"] = csId
        if chunk:
            return doChunk(result)
        return result

    def getStation(self, sid, limit):
        data = self.api.get_station_tracks(sid, num_tracks=limit)
        tracks = []

        for track in data:
            tracks.append({
                "id": track["nid"],
                "title": track["title"],
                "artist": track["artist"],
                "length": time.strftime('%M:%S', time.gmtime(int(track["durationMillis"]) / 1000)),
                "album": track["album"],
                "albumId": track["albumId"]
            })

        result = {
            "id": sid,
            "name": next(s["name"] for s in self.getStations() if s["id"] == sid),
            "tracks": tracks,
            "total": len(tracks)
        }

        return result

    def getStations(self, chunk=False):
        data = self.api.get_all_stations()
        result = []
        for station in data:
            sd = {
                "name": station["name"],
                "id": station["id"],
                "seed": station["seed"]
            }
            if "compositeArtRefs" in station:
                img = [i["url"] for i in station["compositeArtRefs"] if i["aspectRatio"] == "1"]
                sd["img"] = img[0]
            elif "imageUrls" in station:
                sd["img"] = station["imageUrls"][0]["url"]
            else:
                sd["img"] = ""
            result.append(sd)
        if chunk:
            return doChunk(result)
        return result

    def addStation(self, sid, limit, user):
        try:
            station = self.getStation(sid, limit)
            tracks = []
            for track in station["tracks"]:
                item = order.QueueItem(str(track["id"]), "gmusic")
                if not item.error:
                    tracks.append(item)
            order.bulk_add(tracks, user)
            return True
        except Exception as e:
            log.warn("failed to add station: " + e.message)
            return False

    def deleteAllStations(self):
        data = self.api.get_all_stations()
        for station in data:
            self.api.delete_stations(station["id"])

    def getRecommendations(self, chunk=False):
        data = self.api.get_listen_now_items()
        stations = self.getStations()

        results = []
        for r in data:
            d = {}
            d["type"] = int(r["type"])
            if d["type"] == 3:
                d["typeName"] = "station"

                seedType = int(r["radio_station"]["id"]["seeds"][0]["seedType"])
                if seedType == 1:
                    d["seedType"] = "trackLockerId"
                    d["stationType"] = "trackLockerId"
                elif seedType == 2:
                    d["seedType"] = "trackId"
                    d["stationType"] = "track_id"
                elif seedType == 3:
                    d["seedType"] = "artistId"
                    d["stationType"] = "artist_id"
                elif seedType == 4:
                    d["seedType"] = "albumId"
                    d["stationType"] = "album_id"
                elif seedType == 5:
                    d["seedType"] = "genreId"
                    d["stationType"] = "genre_id"
                elif seedType == 8:
                    d["seedType"] = "playlistShareToken"
                    d["stationType"] = "playlist_token"
                elif seedType == 9:
                    d["seedType"] = "curatedStationId"
                    d["stationType"] = "curated_station_id"

                d["seed"] = r["radio_station"]["id"]["seeds"][0][d["seedType"]]

                d["title"] = r["radio_station"]["title"]
                check = [s["id"] for s in stations if s["name"] == d["title"]]
                if not check:
                    log.debug("creating " + d["title"])
                    csId = self.api.create_station(name=d["title"], **{d["stationType"]: d["seed"]})
                else:
                    csId = check[0]
                d["id"] = csId

            elif d["type"] == 1:
                d["typeName"] = "album"
                d["albumName"] = r["album"]["title"]
                d["artistName"] = r["album"]["artist_name"]
                d["id"] = r["album"]["id"]["metajamCompactKey"]
            else:
                d["typeName"] = "unknown"

            if "compositeArtRefs" in r:
                if "aspectRatio" in r["compositeArtRefs"]:
                    d["img"] = [i["url"] for i in r["compositeArtRefs"] if i["aspectRatio"] == "1"][0]
                else:
                    d["img"] = r["compositeArtRefs"][0]["url"]
            elif "images" in r:
                if "aspectRatio" in r["images"]:
                    d["img"] = [i["url"] for i in r["images"] if i["aspectRatio"] == "1"][0]
                else:
                    d["img"] = r["images"][0]["url"]
            else:
                d["img"] = ""
            d["suggestionText"] = r["suggestion_text"]

            results.append(d)

        if chunk:
            return doChunk(results)
        else:
            return results


def doChunk(l, n=2):
    for i in xrange(0, len(l), n):
        yield l[i:i + n]
