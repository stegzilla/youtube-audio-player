#!/usr/bin/env python
import logging as log
import os
import threading

import config
import web
from db import Player
from handlers import gmusicHandler

log.basicConfig(level=log.INFO,
                format='%(asctime)s:\n'
                       '%(levelname)s: '
                       '%(module)s.'
                       '%(funcName)s-'
                       '%(lineno)d: '
                       '%(message)s')


class Starter:
    def __init__(self):
        self.server = None
        self.player = None
        config.path = os.path.dirname(__file__)

    def start(self):
        config.load()

        self.server = web.server()
        self.server.start()
        for player in Player.select():
            for account in player.accounts:
                if account.source == 'gmusic':
                    gmusicHandler.clients[player.player_id] = gmusicHandler.GMusic(account)

    def run(self):
        thread = threading.Thread(target=self.start)
        thread.start()


main = Starter()
main.run()
