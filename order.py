import logging as log
import os
from datetime import datetime

import pafy

import config
from db import Queue, User
from handlers import gmusicHandler

log.basicConfig(level=log.INFO,
                format='%(asctime)s:\n'
                       '%(levelname)s: '
                       '%(module)s.'
                       '%(funcName)s-'
                       '%(lineno)d: '
                       '%(message)s')

track_list = []
queue = []
currentTrack = None
currentUser = 0
totalQueued = 0
pause = False
skip = False
qid = 1
maxlength = 600
font = "<body style=\"font-family: Helvetica, Arial, sans-serif;\">"


def get_current():
    global currentTrack
    return currentTrack


class QueueItem:
    def __init__(self, tid, source, player):
        try:
            self.id = tid
            self.source = source
            self.error = None
            self.owner = None
            self.isDownloaded = False
            self.downloading = False

            self.artist_name = None
            self.album_name = None
            self.track_name = None
            self.title = None

            if self.source == "yt":
                video = pafy.new(self.id)
                stream = video.getbestaudio(preftype="m4a")
                if not stream:
                    stream = video.getbestaudio(preftype="mp4")
                filename = os.path.join(config.path, "yt", self.id + "." + stream.extension)
                if not os.path.exists(os.path.dirname(filename)):
                    os.mkdir(os.path.dirname(filename))
                if not os.path.isfile(filename):
                    stream.download(filename, quiet=True)
                self.url = filename
                self.title = video.title
                if video.length >= config.conf["misc"]["maxLength"]:
                    self.error = "track too long"
            elif self.source == "gmusic" or "gmusiclibrary":
                info = gmusicHandler.clients[player.player_id].getInfo(tid)
                self.track_name = info['name']
                self.artist_name = info['artist']
                self.album_name = info['album']
                self.title = info["title"]
                self.id = info["id"]
                self.url = None
                if (info["length"] / 1000) >= config.conf["misc"]["maxLength"]:
                    self.error = "track too long"

            self.length = 0.0
            self.pos = 0.0
            self.aclength = "00:00"
            self.acpos = "00:00"
            log.info("queue item created: %s - %s (%s)" % (self.title, self.id, self.source))
        except Exception as e:
            log.error("something went wrong: %s" % (str(e)))


class QueueItemFromDB:
    def __init__(self, track):
        try:
            self.id = track.track_id
            self.qid = track.id
            self.source = track.source
            self.error = None
            self.owner = track.user_id
            self.isDownloaded = False
            self.downloading = False

            self.artist_name = None
            self.album_name = None
            self.track_name = None
            self.title = None

            if self.source == "yt":
                self.url = track.path
                self.title = track.track_name
            elif self.source == "gmusic" or "gmusiclibrary":
                self.artist_name = track.artist_name
                self.album_name = track.album_name
                self.title = u'{0} - {1} ({2})'.format(track.artist_name, track.track_name, track.album_name)
                self.url = None

            self.length = 0.0
            self.pos = 0.0
            self.aclength = "00:00"
            self.acpos = "00:00"
            log.info("queue item created from db: %s - %s (%s)" % (self.title, self.id, self.source))
        except Exception as e:
            log.error("something went wrong: %s" % (str(e)))


def get_next(player):
    next_user = User.select().order_by(User.last_played.asc()).limit(1)[0]
    track = Queue.get(user_id=next_user.id, player=player)
    #Queue.delete_by_id(track.qid)
    global currentUser
    currentUser = track.user
    next_user.last_played = datetime.utcnow()
    next_user.save()

    #update_queue()
    return track


def add(track_id, source, user, player):
    if source == 'yt':
        video = pafy.new(track_id)

        return Queue.create(
            track_id=track_id,
            track_name=video.title,
            path=None,
            source=source,
            user=user,
            player=player
        )
    elif source in ['gmusic', 'gmusiclibrary']:
        info = gmusicHandler.clients[player].getInfo(track_id)

        return Queue.create(
            track_id=info['id'],
            track_name=info['title'],
            artist_name=info['artist'],
            album_name=info['album'],
            path=None,
            source=source,
            user=user,
            player=player
        )


def bulk_add(tracks, user):
    for track in tracks:
        add(track, user, update=False)
    #update_queue()


def remove(track_id):
    Queue.delete_by_id(track_id)
    #update_queue()


def rotate(l, n):
    return l[-n:] + l[:-n]


def update_queue():
    user_order = [user.id for user in User.select().order_by(User.last_played.asc())]
    tmp_tracks = list(Queue.select())
    i = 1
    while tmp_tracks:
        for u in user_order:
            try:
                track = next((j, t) for j, t in enumerate(tmp_tracks) if t.user_id == u)
                track[1].order = i
                track[1].save()
                i += 1
                tmp_tracks.pop(track[0])
            except StopIteration:
                pass


def get_queue():
    user_order = [user.id for user in User.select().order_by(User.last_played.asc())]
    tmp_tracks = list(Queue.select())
    tracks = []
    while tmp_tracks:
        for u in user_order:
            try:
                track = next(j for j, t in enumerate(tmp_tracks) if t.user_id == u)
                tracks.append(tmp_tracks.pop(track))
            except StopIteration:
                pass
    return tracks
