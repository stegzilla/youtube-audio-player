from apiclient.discovery import build
from apiclient.errors import HttpError

DEVELOPER_KEY = 'AIzaSyAwku29ZvZHnBRNQouAAdU0Y7j-drFCHio'
YOUTUBE_API_SERVICE_NAME = 'youtube'
YOUTUBE_API_VERSION = 'v3'


def search(query):
    try:
        youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
                        developerKey=DEVELOPER_KEY)

        search_response = youtube.search().list(
            q=query,
            part='id,snippet',
            type='video',
            maxResults=50,
        ).execute()

        videos = []

        for search_result in search_response.get('items', []):
            videos.append({
                'id': search_result['id']['videoId'],
                'title': search_result['snippet']['title']
            })
        return videos
    except HttpError, e:
        print 'An HTTP error {0} occurred:\n{1}'.format(e.resp.status, e.content)
