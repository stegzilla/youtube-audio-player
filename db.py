import json
import os
from datetime import datetime

from peewee import *
from playhouse.db_url import connect

db = connect(os.environ.get('DATABASE_URL') or 'sqlite:///music.db')


class BaseModel(Model):
    class Meta:
        database = db


class TrackModel(BaseModel):
    track_id = CharField()
    track_name = CharField()
    artist_name = CharField(null=True)
    album_name = CharField(null=True)
    source = CharField()


class JSONField(TextField):
    def db_value(self, value):
        return json.dumps(value)

    def python_value(self, value):
        return json.loads(value)


class Player(BaseModel):
    player_id = CharField(primary_key=True)
    name = CharField(unique=True)
    heartbeat = DateTimeField()


class User(BaseModel):
    username = CharField(unique=True)
    name = CharField()
    last_played = DateTimeField(default=datetime.fromtimestamp(1))
    default_player = ForeignKeyField(Player)


class Session(BaseModel):
    session = CharField(primary_key=True)
    user = ForeignKeyField(User, backref='sessions')


class Queue(TrackModel):
    user = ForeignKeyField(User, backref='queued_tracks')
    player = ForeignKeyField(Player, backref='queued_tracks')


class Current(TrackModel):
    player = ForeignKeyField(Player, backref='current_track')
    user = ForeignKeyField(User, backref='current_track')
    length = FloatField(default=0.0)
    pos = FloatField(default=0.0)
    paused = BooleanField(default=False)
    should_pause = BooleanField(default=False)
    skip = BooleanField(default=False)


class Favourite(TrackModel):
    public = BooleanField()
    user = ForeignKeyField(User, backref='favourite_tracks')


class Account(BaseModel):
    player = ForeignKeyField(Player, backref='accounts')
    source = CharField()
    username = CharField()
    password = CharField()
    extras = JSONField()


class Config(BaseModel):
    key = CharField(primary_key=True)
    value = CharField()
