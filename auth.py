import hashlib
from datetime import datetime
from time import mktime

import requests

from db import User, Session, Player, Config


def ldap_login(username, password):
    try:
        r = requests.post(Config.get_by_id('auth_server').value, {'username': username, 'password': password})
        data = r.json()
        if data['success']:
            name = data['name']
            u = User.get_or_none(username=username)
            if not u:
                u = User.create(username=username, name=str(name), default_player=Player.get())
                u.save()
            return True, u
    except Exception as e:
        print e
    return False, ''


def add_session(user):
    now = mktime(datetime.now().timetuple())
    s = hashlib.md5("music" + user.username + str(now)).hexdigest()
    session = Session.create(session=s, user=user)
    session.save()
    return s


def get_session(s):
    try:
        return Session.get_by_id(s)
    except Session.DoesNotExist:
        return None


def remove_session(s):
    Session.delete_by_id(s)
