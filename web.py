# -*- coding: utf-8 -*-
import re

import tornado.ioloop
import tornado.web
import sys, os.path
import threading
import order
import pafy
from playhouse.shortcuts import model_to_dict, dict_to_model
import yt
import time
import logging as log

from db import User, Queue, Player, Current
from handlers import gmusicHandler
import config
import json
import auth

log.basicConfig(level=log.INFO,
                format='%(asctime)s:\n'
                       '%(levelname)s: '
                       '%(module)s.'
                       '%(funcName)s-'
                       '%(lineno)d: '
                       '%(message)s')


class NoApiLoggingFilter(log.Filter):
    def __init__(self, name="NoApi200LoggingFilter"):
        log.Filter.__init__(self, name)

    def filter(self, record):
        message = record.getMessage()
        if message.startswith("200 POST /api"):
            return False
        elif message.startswith("403 POST /api"):
            return False
        return True


log.getLogger("tornado.access").addFilter(NoApiLoggingFilter())

query_blacklist = []
api_response = json.loads('{"result":"","data":{}}')
default_player = Player.get_by_id('up')


class server():
    def __init__(self):
        self.is_closing = False
        self.port = int(os.environ.get('PORT', 8888))
        self.application = tornado.web.Application([
            (r"/", main),
            (r"/search", search),
            (r"/playlists", playlists),
            (r"/playlist/(gmusic|gmusiclibrary)-(.*)", playlist),
            (r"/situations", situations),
            (r"/recommendations", recommendations),
            (r"/stations", stations),
            (r"/stations/(gmusic|gmusiclibrary)-(.*)", station),
            (r"/api", api),
            (r"/api/current", apiCurrent),
            (r'/api/player', ApiPlayer),
            (r'/api/player/get_next', ApiGetNext),
            (r"/album/(gmusic|gmusiclibrary)-(.*)", album),
            (r"/queue", queue),
            (r"/static/(.*)", tornado.web.StaticFileHandler, {"path": os.path.dirname(__file__) + "/static"}),
            (r"/login", login),
            (r"/logout", logout)
        ])

        if not __file__:
            path = sys.argv[0]
        else:
            path = __file__

        self.settings = dict(
            template_path=os.path.join(os.path.dirname(path), "templates"),
            debug=config.conf["web"]["debug"],
            cookie_secret="music123",
            login_url="/login"
        )
        self.application.settings = self.settings

    def doStart(self):
        log.info("starting webserver")
        self.application.listen(self.port)
        tornado.ioloop.IOLoop.instance().start()

    def start(self):
        self.thread = threading.Thread(target=self.doStart)
        self.thread.start()

    def stop(self):
        log.info("stopping webserver")
        tornado.ioloop.IOLoop.instance().stop()
        log.info("stopped webserver")


class BaseHandler(tornado.web.RequestHandler):
    @property
    def providers(self):
        p = ["yt"]
        if gmusicHandler.clients:
            p.append("gmusiclibrary")
            if gmusicHandler.clients[default_player.player_id].api.is_subscribed:
                p.append("gmusic")

        return p

    content = dict(
        error=None
    )

    def get_template_namespace(self):
        ns = super(BaseHandler, self).get_template_namespace()
        current = order.get_current()
        currentUser = None
        if current:
            currentUser = User.get_by_id(current.owner).name
        ns.update({
            "current": current,
            "currentUser": currentUser,
            "currentPaused": order.pause,
            "providers": self.providers,
            "user": self.current_user.user.name if self.current_user else None,
            "appTitle": config.conf['web']['title']
        })

        return ns

    def get_current_user(self):
        cookie = self.get_cookie("session")
        if not cookie:
            return None
        session = auth.get_session(cookie)
        if session:
            return session
        return None

    def prepare(self):
        if ('X-Forwarded-Proto' in self.request.headers and self.request.headers['X-Forwarded-Proto'] != 'https'):
            self.redirect(re.sub(r'^([^:]+)', 'https', self.request.full_url()))


class main(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        self.redirect("/queue")
        return


class queue(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        queue = []
        for item in order.get_queue():
            title = item.track_name
            if item.source in ['gmusic', 'gmusiclibrary']:
                title = u'{0} - {1} ({2})'.format(item.artist_name, item.track_name, item.album_name)
            queue.append({
                "qid": item.id,
                "title": title,
                "owner": item.user.name,
                "canDelete": item.user.id == self.current_user.user.id
            })
        self.content["queue"] = queue
        self.render("queue.html", **self.content)


"""class pause(tornado.web.RequestHandler):
    @tornado.web.authenticated
    def get(self):
        if config.conf["web"]["whitelist"]:
            if self.request.remote_ip not in config.conf["web"]["whitelist"]:
                self.write(order.font + "not for you</br><a href=\"/\">return</a>")
                return
        if order.pause == False:
            order.pause = True
            self.write(order.font + "pausing now</br><a href=\"/\">return</a>")
            log.info("pause requested")
        else:
            order.pause = False
            self.write(order.font + "resuming now</br><a href=\"/\">return</a>")
            log.info("resume requested")


class skip(tornado.web.RequestHandler):
    @tornado.web.authenticated
    def get(self):
        if config.conf["web"]["whitelist"]:
            if self.request.remote_ip not in config.conf["web"]["whitelist"]:
                self.write(order.font + "not for you</br><a href=\"/\">return</a>")
                return
        if config.conf["web"]["restrictSkip"]:
            if not self.request.remote_ip == order.currentTrack.owner:
                self.write(order.font + "you can only skip your own tracks</br><a href=\"/\">return</a>")
                return
        order.skip = True
        self.write(order.font + "skipping now</br><a href=\"/\">return</a>")
        log.info("skip requested")


class addyt(tornado.web.RequestHandler):
    @tornado.web.authenticated
    def get(self, ytid):
        if config.conf["web"]["whitelist"]:
            if self.request.remote_ip not in config.conf["web"]["whitelist"]:
                self.write(order.font + "not for you</br><a href=\"/\">return</a>")
                return
        log.info("starting queue add")
        if len(ytid) != 11:
            response = order.font + "invalid youtube id: " + ytid
        else:
            self.write(order.font + "searching...</br>")
            self.flush()
            item = order.QueueItem(ytid, "yt")
            if not item.error:
                response = "adding %s (%s)" % (item.title, item.id)
                order.add(item, self.current_user.user.username)
                log.info("added %s (%s)" % (item.title, item.id))
                response += "</br>added!"
            else:
                response = "error adding %s:</br>" % (item.title)
                response += item.error

            response += "</br><a href=\"/\">return</a>"
        self.write(response)


class delete(tornado.web.RequestHandler):
    @tornado.web.authenticated
    def get(self, qid):
        if config.conf["web"]["whitelist"]:
            if self.request.remote_ip not in config.conf["web"]["whitelist"]:
                self.write(order.font + "not for you</br><a href=\"/\">return</a>")
                return
        log.info("starting queue remove")
        deleted = False
        current = False

        for owner in order.track_list:
            for item in owner.list:
                if item.qid == int(qid):
                    response = order.font + "deleting %s (qID: %s)" % (item.title, item.qid)
                    if item == order.currentTrack:
                        current = True
                        continue
                    else:
                        order.remove(item)
                        response += "</br>deleted!"
                        deleted = True
                        break

        if deleted == False and current == True:
            response += "</br>cannot delete current item. use skip instead"
        elif deleted == False and current == False:
            response += "</br>%s not found" % (qid)

        response += "</br><a href=\"/\">return</a>"
        self.write(response)
"""

class search(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        self.render("search.html", **self.content)


class playlists(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        playlists = {}
        if "gmusiclibrary" in self.providers:
            playlists["gmusiclibrary"] = gmusicHandler.getPlaylists()

        self.content["playlists"] = playlists

        self.render("playlists.html", **self.content)


class situations(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        if config.conf["gmusic"]["enable"] == True:
            self.render("situations.html", situations=gmusicHandler.getSituations(chunk=True), **self.content)


class recommendations(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        if config.conf["gmusic"]["enable"] == True:
            self.render("recommendations.html", recommendations=gmusicHandler.getRecommendations(chunk=True),
                        **self.content)


class stations(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        if config.conf["gmusic"]["enable"] == True:
            self.render("stations.html", stations=gmusicHandler.getStations(chunk=True), **self.content)


class station(BaseHandler):
    @tornado.web.authenticated
    def get(self, source, sId):
        if source == "gmusic" or "gmusiclibrary":
            station = gmusicHandler.getStation(sId, limit=3)
        else:
            self.write(source + " is invalid")
            return

        self.content["id"] = station["id"]
        self.content["tracks"] = station["tracks"]

        self.render("station.html", **self.content)


class playlist(BaseHandler):
    @tornado.web.authenticated
    def get(self, source, pId):
        if source == "gmusic" or "gmusiclibrary":
            playlist = gmusicHandler.getPlaylist(pId, source)
        else:
            self.write(source + " is invalid")
            return

        self.content["source"] = source
        self.content['playlist'] = playlist

        self.render("playlist.html", **self.content)


class login(BaseHandler):
    def get(self):
        self.render("login.html", error=False)

    def post(self):
        username = self.get_argument("user").lower()
        password = self.get_argument("password")
        login, user = auth.ldap_login(username, password)
        if login:
            s = auth.add_session(user)
            self.set_cookie("session", s, expires_days=None)
            if self.get_argument("next") == "undefined":
                self.redirect("/")
            else:
                self.redirect(self.get_argument("next"))
            return
        else:
            self.render("login.html", error=True)


class logout(BaseHandler):
    def get(self):
        if self.current_user:
            self.clear_cookie("session")
            auth.remove_session(self.current_user.session)
        self.redirect("/")
        return


class apiCurrent(BaseHandler):
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Methods", "get, post, options")
        self.set_header("Access-Control-Allow-Headers", "Content-Type")

    def options(self):
        return

    def get(self):
        self.write(order.font + "nope")
        return

    def post(self):
        try:
            request = tornado.escape.json_decode(self.request.body)

            if request["method"] == "current":
                data = {"current": {}}
                current = order.currentTrack
                currentData = {}
                if current:
                    # for savedName in config.conf["web"]["ipNames"]:
                    #	if savedName["ip"] == current.owner:
                    #		current.owner = savedName["name"]
                    currentData = {
                        "title": current.title,
                        "position": current.acpos,
                        "length": current.aclength,
                        "paused": order.pause,
                        "owner": User.get_by_id(current.owner).name
                    }
                data["current"] = currentData

                response = api_response
                response["result"] = "success"
                response["data"] = data
                self.write(response)
            else:
                raise Exception("not valid request")
        except Exception as e:
            response = api_response
            response["result"] = "error"
            response["data"] = str(e)
            self.write(response)


class api(BaseHandler):
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Methods", "get, post, options")
        self.set_header("Access-Control-Allow-Headers", "Content-Type")

    def options(self):
        return

    @tornado.web.authenticated
    def get(self):
        self.write(order.font + "nope")
        return

    @tornado.web.authenticated
    def post(self):
        try:
            request = tornado.escape.json_decode(self.request.body)

            if request["method"] == "queue":
                queue = []
                for item in order.get_queue():
                    title = item.track_name
                    if item.source in ['gmusic', 'gmusiclibrary']:
                        title = u'{0} - {1} ({2})'.format(item.artist_name, item.track_name, item.album_name)
                    queue.append({
                        "qid": item.id,
                        "title": title,
                        "owner": item.user.name,
                        "canDelete": item.user.id == self.current_user.user.id
                    })

                data = {"queue": queue}

            elif request["method"] == "pause":
                if not order.pause:
                    order.pause = True
                    data = {"paused": True}
                else:
                    order.pause = False
                    data = {"paused": False}

            elif request["method"] == "delete":
                deleted = False
                current = False
                qid = request["params"]["id"]

                order.remove(qid)
                deleted = True
                response = api_response

                if not deleted and current:
                    raise Exception({"message": "cannot delete current item. use skip instead"})
                elif not deleted and not current:
                    raise Exception({"message": "%s not found" % (qid)})
                else:
                    data = {"deleted": True}

            elif request["method"] == "skip":
                order.skip = True
                data = {"skipped": True}

            elif request["method"] == "search":
                web = False
                data = {"results": []}
                if request["params"]["source"] == "yt":
                    results = yt.search(request["params"]["query"])
                    for result in results:
                        trackResult = {
                            "id": result["id"],
                            "title": result["title"]
                        }
                        # trackResult = '{"id":\"%s\","title":\"%s\"}' % (result["id"], result["title"].replace("\"", "\\\""))
                        data["results"].append(trackResult)
                    data["count"] = len(data["results"])
                elif request["params"]["source"] == "gmusic":
                    if not gmusicHandler.clients[default_player.player_id].api:
                        raise Exception("gmusic disabled")
                    results = gmusicHandler.clients[default_player.player_id].search(request["params"]["query"])
                    for track in results:
                        trackResult = {
                            "id": track["id"],
                            "title": track["title"],
                            "artist": track["artist"],
                            "album": track["album"],
                            "albumId": track["albumId"],
                            "length": time.strftime('%M:%S', time.gmtime(int(track["duration"] / 1000))),
                            "explicit": int(track["explicit"])
                        }
                        data["results"].append(trackResult)
                    data["count"] = len(data["results"])
                elif request["params"]["source"] == "gmusiclibrary":
                    if not gmusicHandler.clients[default_player.player_id].api:
                        raise Exception("gmusic disabled")
                    results = gmusicHandler.clients[default_player.player_id].searchLibrary(request["params"]["query"])
                    for track in results:
                        trackResult = {
                            "id": track["id"],
                            "title": track["title"],
                            "artist": track["artist"],
                            "album": track["album"],
                            "albumId": track["albumId"],
                            "length": time.strftime('%M:%S', time.gmtime(int(track["duration"] / 1000))),
                            "explicit": int(track["explicit"])
                        }
                        data["results"].append(trackResult)
                    data["count"] = len(data["results"])

            elif request["method"] == "add":
                data = json.loads('{"added":false,"info":""}')
                if request["params"]["source"] in ["gmusic", "gmusiclibrary", "yt"]:
                    track = order.add(request['params']['id'], request['params']['source'], self.current_user.user, default_player.player_id)
                    data["info"] = u"{0} added".format(track.track_name)
                    data["added"] = True
                else:
                    raise Exception('{0} not a vaild source'.format(request['params']['source']))

            elif request["method"] == "addStation":
                data = json.loads('{"added":false,"info":""}')
                if request["params"]["source"] == "gmusic":
                    if gmusicHandler.addStation(request["params"]["id"], request["params"]["count"],
                                                self.current_user.user):
                        data["added"] = True
                    else:
                        raise Exception("failed to add station tracks")

            elif request["method"] == "addAlbum":
                data = json.loads('{"added":false,"info":""}')
                if request["params"]["source"] == "gmusic" or "gmusiclibrary":
                    if gmusicHandler.addAlbum(request["params"]["id"], request["params"]["source"],
                                              self.current_user.user):
                        result = gmusicHandler.getAlbum(request["params"]["id"], request["params"]["source"])
                        data["info"] = "%s - %s added" % (result["artist"], result["albumName"])
                        data["added"] = True
                    else:
                        raise Exception("failed to add album")

            elif request["method"] == "addPlaylist":
                data = json.loads('{"added":false,"info":""}')
                if request["params"]["source"] == "gmusic" or "gmusiclibrary":
                    if gmusicHandler.addPlaylist(request["params"]["id"], request["params"]["source"],
                                                 self.current_user.user):
                        result = gmusicHandler.getPlaylistLite(request["params"]["id"], request["params"]["source"])
                        data["info"] = "%s added" % (result["name"])
                        data["added"] = True
                    else:
                        raise Exception("failed to add playlist")
            else:
                data = "invalid method: " + request["method"]
            response = api_response
            response["result"] = "success"
            response["data"] = data
            self.write(response)
        except Exception as e:
            response = api_response
            response["result"] = "error"
            response["data"] = str(e)
            self.write(response)


class album(BaseHandler):
    @tornado.web.authenticated
    def get(self, source, aId):
        if source == "gmusic" or "gmusiclibrary":
            album = gmusicHandler.getAlbum(aId, source)
        else:
            self.write(source + " is invalid")
            return

        if album["id"] == 0:
            self.content["error"] = "Album not found"
            self.content["artist"] = None
            self.content["albumName"] = None
            self.content["albumId"] = None
            self.content["total"] = None
            self.content["albumArt"] = None
            self.content["tracks"] = None
            self.content["source"] = None
        else:
            self.content["error"] = None
            self.content["artist"] = album["artist"]
            self.content["albumName"] = album["albumName"]
            self.content["albumId"] = album["id"]
            self.content["total"] = int(album["total"])
            self.content["albumArt"] = album["albumArt"]
            self.content["tracks"] = album["tracks"]
            self.content["source"] = source

        self.render("album.html", **self.content)


class ApiPlayer(BaseHandler):
    def post(self):
        removeme = {
            'id': '',
            'state': '',
            'timestamp': 123,
            'current': {
                'track_id': '',
                'source': '',
                'state': '',
                'position': 123,
                'length': 123
            }
        }
        try:
            request = tornado.escape.json_decode(self.request.body)
            player = Player.get_by_id(request['id'])
            player.state = request['state']
            player.heartbeat = request['timestamp']
            if request['current']:
                player.current = True


        except KeyError, Player.DoesNotExist:
            self.write_error(500)


class ApiFinishTrack(BaseHandler):
    def post(self):
        request = tornado.escape.json_decode(self.request.body)
        player = Player.get_by_id(request['id'])


class ApiGetNext(BaseHandler):
    def post(self):
        request = tornado.escape.json_decode(self.request.body)
        player = Player.get_by_id(request['id'])
        track = order.get_next(player)
        new_track = Current.create(
            track_id=track.track_id,
            track_name=track.track_name,
            artist_name=track.artist_name,
            album_name=track.album_name,
            source=track.source,
            player=track.player,
            user=track.user
        )

        current = model_to_dict(new_track)
        del current['player']
        current['user'] = current['user']['id']

        if current['source'] == 'yt':
            pass
        elif current['source'] in ['gmusic', 'gmusiclibrary']:
            current['url'] = gmusicHandler.getStreamUrl(current['track_id'])
        self.write(tornado.escape.json_encode(current))
