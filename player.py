#!/usr/bin/env python
import logging as log
import threading
import time

import mplayer

import config
import order
from handlers import gmusicHandler

log.basicConfig(level=log.INFO,
                format='%(asctime)s:\n'
                       '%(levelname)s: '
                       '%(module)s.'
                       '%(funcName)s-'
                       '%(lineno)d: '
                       '%(message)s')


class Player:
    def __init__(self, queue_item):
        self.item = queue_item
        self.status = 0
        self.player_out = open("mplayer.out", "w")
        self.player = mplayer.Player(stderr=self.player_out, args=("-cache 10240"))
        self.current = None
        self.skip_flag = False

    def play_url(self):
        p = self.player
        try:
            item = self.item
            order.currentTrack = item
            self.current = item.title
            log.info(u"loading {0}".format(item.title))
            p.loadfile(item.url)
            while not p.path:
                time.sleep(1)

            self.status = 1
            if p.version.startswith("1") or p.version.startswith('R'):
                p.pause()

            while p.time_pos is None:
                time.sleep(0.5)

            while (p.time_pos <= p.length) and (p.time_pos and p.length):
                if self.skip_flag:
                    self.skip_flag = False
                    break
                item.pos = p.time_pos
                if not item.pos:
                    item.pos = 0
                item.length = p.length
                if not item.length:
                    item.length = 0
                item.acpos = time.strftime('%M:%S', time.gmtime(int(item.pos)))
                item.aclength = time.strftime('%M:%S', time.gmtime(int(item.length)))
                order.currentTrack = item
                log.info(u"qid: %i, title: %s, state: %s, pos: %s/%s" % (
                    item.qid, item.title[:20], self.status, item.acpos, item.aclength))
                time.sleep(1)
        except Exception as e:
            log.error("something went wrong: %s" % (str(e)))

        log.info("playback ends")
        p.quit()
        self.player_out.close()
        self.current = None
        order.currentTrack = None
        self.status = 0

    def prepare_play(self):
        return

    def play(self):
        self.status = 3
        self.prepare_play()
        thread = threading.Thread(target=self.play_url)
        thread.start()

    def pause(self):
        p = self.player
        if self.status == 1:
            # pause
            p.pause()
            self.status = 2
            log.info("playback paused")
        elif self.status == 2:
            # unpause
            p.pause()
            self.status = 1
            log.info("playback resumed")

    def skip(self):
        if order.skip:
            log.info("skipping track")
            self.skip_flag = True
            order.skip = False

    @property
    def playing(self):
        if self.status == 1:
            return True
        else:
            return False

    @property
    def paused(self):
        if self.status == 2:
            return True
        else:
            return False

    @property
    def pending(self):
        if self.status == 3:
            return True
        else:
            return False

    @property
    def stopped(self):
        if self.status == 0:
            return True
        else:
            return False

    @property
    def state(self):
        return self.status


class Youtube(Player):
    def prepare_play(self):
        return


class GMusic(Player):
    def prepare_play(self):
        if not config.conf["gmusic"]["download"]:
            self.item.url = gmusicHandler.getStreamUrl(self.item.id)
